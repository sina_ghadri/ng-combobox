### Welcome

 display items in Combobox (single & multiple).

![alt text](screenshots/01.png)
![alt text](screenshots/02.png)

### Usage
```html
<combobox [items]="items" [selectedItem]="selectedItem"></combobox>
```
```html
<combobox-multiple [items]="items" [selectedItems]="selectedItems"></combobox-multiple>
```
### Features
Two way binding on selectedItem & selectedItems
### Examples

##### *.component.ts
```typescript
@Component(/**/)
export class Component implements OnInit {
  selectedItem:any = { value: null, text: '' };
  selectedItems:any[] = [];
  items: any[];
  
  public constructor(private service: Service) { }
  
  ngOnInit(): void { 

    this.items = [
            { value: 1, text: 'sina ghadri' },
            { value: 2, text: 'ali moslem' },
            { value: 3, text: 'Sara Ezmir' },
            { value: 4, text: 'ali pasha' },
            { value: 5, text: 'farnoosh bagheri' },
            { value: 7, text: 'Artin Garshasbi' },
        ]
  }

   selectedItem() {
   
    console.log(this.selectedItem);
  }
  selectedItems() {
    
    console.log(this.selectedItems);
  }
}
```
##### *.component.html
```html
<combobox [items]="items" [selectedItem]="selectedItem"></combobox>
<combobox-multiple [items]="items" [selectedItems]="selectedItems"></combobox-multiple>

 <div><button (click)="selectedItem()">selectedItem</button></div>
 <div><button (click)="selectedItems()">selectedItems</button></div>
```
