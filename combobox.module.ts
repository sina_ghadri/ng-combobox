import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ComboboxSingleComponent } from './single/single.component';
import { ComboboxMultipleComponent } from './multiple/multiple.component';

@NgModule({
  imports: [FormsModule,BrowserModule,],
  declarations: [ComboboxSingleComponent,ComboboxMultipleComponent],
  exports: [ComboboxSingleComponent,ComboboxMultipleComponent]
})
export class ComboboxModule { }
